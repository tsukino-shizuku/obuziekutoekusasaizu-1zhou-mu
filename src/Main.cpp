# include "Framework.hpp"
# include "Asset.hpp"
//=============================================================
//
//  メイン関数
//
//=============================================================
void Main()
{
    Asset::font();
    Asset::texture();
    
    Framework framework;
    while (System::Update())
    {
        if (framework.update()) break;
    }
}
