#pragma once

#ifndef __MAINSCENE_HPP__
#define __MAINSCENE_HPP__

# include "Common.hpp"

class MainScene : public MyApp::Scene
{
public:
    //初期化・解放
    MainScene(const InitData& init);
    ~MainScene();

    //更新
    void update() override;
    
    //描画
    void draw() const override;
protected:
    
private:
    
};

#endif /* __MAINSCENE_HPP__ */
