thisPath = left(wscript.scriptfullname, len(wscript.scriptfullname) - len(wscript.scriptname))
Execute(CreateObject("Scripting.FileSystemObject").OpenTextFile(thisPath + "ADODB_stream_lineseparator.vbs").ReadAll())
Execute(CreateObject("Scripting.FileSystemObject").OpenTextFile(thisPath + "change_character_code.vbs").ReadAll())

Function deep_run(ByVal filepath, ByRef fso)
    deep_run = True

    Dim fsoFolder, file, folder, extension, changer, name
    Set fsoFolder = fso.GetFolder(filepath)
    For Each file In fsoFolder.Files
        
        If Not change_character_code(file, "UTF-8", vbLf, "Shift_JIS", vbCrLf) Then
            WScript.Echo "[FAILURE] " + file + " change character code. UTF-8 -> SJIS"
            deep_run = False
        End If
        
        'hpp files only, change extension
        extension = fso.GetExtensionName(file)
        If StrComp(extension, "hpp", 0) = 0 Then
            Set changer = fso.GetFile(file)
            name = right(file, len(changer.Name))
            name = left(name, len(name) - len("hpp"))
            changer.Name = name + "h"
            Set changer = Nothing
        End If
    Next

    For Each folder In fsoFolder.subfolders
        If Not StrComp(filepath + "\" + folder.name + "\", thisPath, 0) = 0 Then
            If Not deep_run(filepath + "\" + folder.name, fso) Then
                deep_run = False
            End If
        End If
    Next
    
    Set fsoFolder = Nothing
    Set file = Nothing
    Set folder = Nothing
End Function

Dim objFSO: Set objFSO = WScript.CreateObject("Scripting.FileSystemObject")
Dim parentFolderName: parentFolderName = objFSO.GetParentFolderName(thisPath)

'We're going to go deep into the directory.
If deep_run(parentFolderName,objFSO) Then
    WScript.Echo "OK"
Else
    WScript.Echo "Please, Do not move this tool twice."
End if