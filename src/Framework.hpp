#pragma once

#ifndef __FRAMEWORK_HPP__
#define __FRAMEWORK_HPP__

# include "Common.hpp"

// シーン管理クラス
class Framework
{
public:
    Framework();
    virtual ~Framework(){}
    
    bool update(){ return not manager.update(); }
private:
    //画面サイズ
    static constexpr Size window_size = Size(640,480);
    //FPSの設定
    static constexpr double fps = 60.0;
    //シーンマネージャー
    MyApp manager;
};

#endif /* __FRAMEWORK_HPP__ */
