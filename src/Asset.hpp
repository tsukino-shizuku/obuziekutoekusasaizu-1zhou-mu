#pragma once

#ifndef __ASSET_HPP__
#define __ASSET_HPP__

# include "Common.hpp"

class Asset
{
public:
    static void font();     //フォント
    static void texture();  //画像
private:
    //インスタンスを禁止にする
    Asset();
    ~Asset();
};

#endif /* __ASSET_HPP__ */
