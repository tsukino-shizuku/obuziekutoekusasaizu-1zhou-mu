#include "Asset.hpp"

void Asset::font()
{
    FontAsset::Register(U"Title", 120, U"example/font/AnnyantRoman/AnnyantRoman.ttf");
    FontAsset::Register(U"Menu", 30, Typeface::Regular);
    FontAsset::Register(U"Score", 36, Typeface::Bold);
}

void Asset::texture()
{
    TextureAsset::Register(U"player", U"example/Player.png", TextureDesc::Mipped);
}
