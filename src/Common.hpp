#pragma once

#ifndef __COMMON_HPP__
#define __COMMON_HPP__

# include <Siv3D.hpp>

// シーンの名前
enum class SceneNames
{
    MainScene,
};

/*
// ゲームデータ
struct GameData
{
    int32 highScore = 0; // ハイスコア
};
 */

// シーン管理クラス
//using MyApp = SceneManager<SceneNames, GameData>;
using MyApp = SceneManager<SceneNames>;

#endif /* __COMMON_HPP__ */
