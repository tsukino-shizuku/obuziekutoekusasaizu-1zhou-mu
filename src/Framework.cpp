#include "Framework.hpp"
#include "Scene/MainScene.hpp"

Framework::Framework()
{
    // タイトルを初期化
    Window::SetTitle(U"オブジェクト指向エキササイズ");
    // フレームを表示する
    Window::SetStyle(WindowStyle::Fixed);
    // シーンの大きさを変更
    Window::Resize(window_size);
    // 背景色を設定
    //Scene::SetBackground(ColorF(0.0, 0.0, 0.0)); //黒
    Scene::SetBackground(Palette::Black);
    // フレームレイトを固定
    Graphics::SetTargetFrameRateHz(fps);
    
    // シーンと遷移
    manager.add<MainScene>(SceneNames::MainScene);
    // フェードイン・フェードアウト時の画面の色
    manager.setFadeColor(Palette::Black);
}
